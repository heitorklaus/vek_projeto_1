  // Show Hide divs
 
  function showForm(val){

    $("#preencher").toggle();

} 






// Para CNPJ validando ou não

$(document).ready(function () {
    
    $('.validate').cpfcnpj({
        mask: true,
        validate: 'cpfcnpj',
        event: 'change',
        handler: '.btn2',
        ifValid: function (input) { },
        ifInvalid: function (input) { 

            alert('CNPJ ou CPF Inválido!') 

            $("#inputCPF_CNPJ").val('');
            $("#inputCPF_CNPJ").focus();
            
         }
    });
});





// Gerando uma matriz com os valores da tabela do banco de dados tb_ramo_atividades da linha selecionada no SELECT do html
$("#inputRamo").change(function() {

    
    


    var ramoValores = $("#inputRamo").val();
    var jsonMatriz = JSON.parse("[" + ramoValores + "]");

    // setando de forma global
      window.id = jsonMatriz[0];
      window.limit_debito = jsonMatriz[1];
      window.limit_credito = jsonMatriz[2];   

 

// se select do HTML for setado para opcao "escolher" ou usuario nao tiver feito nada
if($("#inputRamo").val() == '') {
      
    $('#inputTaxaDebito').val('');
    $('#inputDescontoDebito').val('');
    $('#inputTaxaCredito').val('');
    $('#inputDescontoCredito').val('');
    
    $('#inputTaxaDebito').attr('disabled', 'disabled');
    $('#inputDescontoDebito').attr('disabled', 'disabled');
    $('#inputTaxaCredito').attr('disabled', 'disabled');
    $('#inputDescontoCredito').attr('disabled', 'disabled');  

    
}else {
    
      $('#inputTaxaDebito').removeAttr('disabled');
      $('#inputTaxaCredito').removeAttr('disabled');

      $('#inputTaxaDebito').val('');
      $('#inputDescontoDebito').val('');
      $('#inputTaxaCredito').val('');
      $('#inputDescontoCredito').val('');
      
    
      $("#inputTaxaDebito").focus();

    }
});



function calcula(taxa, desconto) {

    // fazendo limpeza do "%" e substituindo "," por "." para fazer a conta
    var taxa = taxa.replace(',', '.').replace('%', '');

    var desconto = desconto.replace(',', '.').replace('%', '') / 100;

    var resultado = taxa * desconto;

    // aquele velho hack para mostrar duas casas pós virgula
    resultado = resultado.toString().split('.');

    var res = resultado[1].slice(0, 2);

    resultado = resultado[0] + '.' + res;

    resultado = taxa - resultado;

    return resultado;



} 

 
 


// passando para a  A FUNCAO "CALCULA" o valor do inputTaxaDebito e inputDescontoDebito E RETORNO UM VALOR JA COMPARANDO ELE
$("#inputDescontoDebito").change(function() { 
    
    var valorDebitoCalculado =  calcula($("#inputTaxaDebito").val(),$("#inputDescontoDebito").val());
    valorDebitoCalculado = valorDebitoCalculado.toString().split('.');
   
    var res_debito = valorDebitoCalculado[1].slice(0, 2);
    valorDebitoCalculado = valorDebitoCalculado[0] + '.' + res_debito;



    // se retorno de CALCULA for MENOR que o limit_debito PERMITIDO entao AVISE que não pode este valor e RESETA o campo
    if( calcula($("#inputTaxaDebito").val(),$("#inputDescontoDebito").val()) < window.limit_debito) {

        $("#inputDescontoDebito").val('');
        $("#inputDescontoDebito").focus();

        alert('Atenção \n O Valor mínimo da taxa é de ('+ window.limit_debito +'%) a sua taxa esta em ('+valorDebitoCalculado+'%)');

   // se retorno de CALCULA for >= entao passe adiante
        } else if( calcula($("#inputTaxaDebito").val(),$("#inputDescontoDebito").val()) >= window.limit_debito) {

         

}
 
    
   

});

// ACESSO A FUNCAO "CALCULA" E RETORNO UM VALOR JA COMPARANDO ELE
$("#inputDescontoCredito").change(function() { 
    
   
    var valorCreditoCalculado =  calcula($("#inputTaxaCredito").val(),$("#inputDescontoCredito").val());
    valorCreditoCalculado = valorCreditoCalculado.toString().split('.');
   
    var res_credito = valorCreditoCalculado[1].slice(0, 2);
    valorCreditoCalculado = valorCreditoCalculado[0] + '.' + res_credito;

    // se retorno de CALCULA for MENOR que o limit_credito PERMITIDO entao AVISE que não pode este valor e RESETA o campo
    if( calcula($("#inputTaxaCredito").val(),$("#inputDescontoCredito").val()) < window.limit_credito) {

        $("#inputDescontoCredito").val('');
        $("#inputDescontoCredito").focus();

        alert('O Valor mínimo da taxa é de ('+ window.limit_credito +'%) a sua taxa esta em ('+valorCreditoCalculado+'%)');

   // se retorno de CALCULA for >= entao passe adiante
        } else if( calcula($("#inputTaxaCredito").val(),$("#inputDescontoCredito").val()) >= window.limit_credito) {

         

}

});



// Verificar se tem valor no campo TAXA_DEBITO ou TAXA_Credito para liberar o campo DESCONTO_DEBITO ou DESCONTO_CREDITO
$("#inputTaxaDebito").on('keyup', function (e) {

    if ($("#inputTaxaDebito").val().length >= 1) {

        $('#inputDescontoDebito').removeAttr('disabled');

    } else {

        $('#inputDescontoDebito').attr('disabled', 'disabled');

    }
});

$("#inputTaxaCredito").on('keyup', function (e) {

    if ($("#inputTaxaCredito").val().length >= 1) {

        $('#inputDescontoCredito').removeAttr('disabled');
        
    } else {

        $('#inputDescontoCredito').attr('disabled', 'disabled');

    }
});

