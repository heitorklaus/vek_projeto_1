 
<?php include('php/listagemDados.php');?>
 
<html>
<head>
<title>Vek - Situação 1</title>
<meta name="viewport" content="width=device-width , shrink-to-fit=no, user-scalable=no">
<meta charset="utf-8">

<!-- Última versão CSS compilada e minificada -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">




</head>  
<body style="background-color:#45A6C6"> 

<body>

<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Simulador de propostas</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
       
      
    </div><!--/.nav-collapse -->
  </div>
</nav>


<div class="container">


  <div class="jumbotron">
    
  <div class="form-group row">
     <div class="col-sm-6"  >
      
      <select id="selectConcorrente"  onchange="if(this.value!='')window.open('php/gerarCSV.php?id='+this.value,'_blank')"  style="  margin-top:2%" class="form-control"    >
        <option selected value=''>Download de Proposta...</option>
      

    <?php if(!$total_propostas) { ?>

      <option   value=''>Nenhuma proposta criada => Clique no botão ao lado!</option>

    <?php } else {?>  
     
      <?php do { ?>
            
           <option value="<?php echo $linha_propostas['id']; ?>"> <?php echo $data_banco; ?> - <?php echo $linha_propostas['concorrente']; ?> </option>
   
      <?php 	} while($linha_propostas = mysql_fetch_assoc($dados_propostas)); ?>

      <?php } ?>  



      </select>
    </div>
 
     <div class="col-sm-2"  >
      
      <center><button type="submit" class="btn btn-primary mb-2" style="   margin-top:5%" onclick="showForm('preencher');"> Criar Nova Proposta</button></center> 
    </div>
    
  </div>


  


<!--formulario -->
<div id='preencher' style="display: none"  > 
<form method="POST" action="php/gravarDados.php" data-toggle="modal" id="myForm">
<input type="hidden" id="status" name="status">
  <div class="form-row">
  <div class="form-group col-md-3">
      <label for="inputNome">Nome Concorrente</label>
      <input type="text" class="form-control"  name="inputNome" id="inputNome" required  />
 
    </div>
    <div class="form-group col-md-3">
      <label for="inputCNPJ">CNPJ/CPF</label>
      <input type="text" class="btn2 validate form-control" name="inputCPF_CNPJ" id="inputCPF_CNPJ" required  />
 
    </div>
    <div class="form-group col-md-3">
      <label for="inputTelefone">Telefone</label>
      <input type="text" class="form-control"  name="inputTelefone" id="inputTelefone" required pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}" />

    </div>
    <div class="form-group col-md-3">
      <label for="inputEmail">Email</label>
      <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email" data-error="Email inválido!"  >
      
 
    </div>
  </div>
  
  <div class="form-row">

    <div class="form-group col-md-6">
      <label for="inputRamo" >Ramo de Atividade</label>
      
      <select id="inputRamo" name="inputRamo" class="form-control"   required>
        <option selected value=''>Escolher...</option>

        <?php do { ?>
            

         <!-- NO VALUE ABAIXO  EU CARREGO DA TABELA "tb_ramo_atividade"
          os CAMPOS  (ID,LIMIT_DEBITO, LIMIT_CREDITO) separados por "," PARA FAZER OS CALCULOS DIRETO NO CLIENTE. -->
 
        <option value="<?php echo $linha_ramo['id']; ?>,<?php echo $linha_ramo['limit_debito']; ?>,<?php echo $linha_ramo['limit_credito']; ?>"> <?php echo $linha_ramo['ramo']; ?> </option>

        <?php 	} while($linha_ramo = mysql_fetch_assoc($dados_ramo)); ?>

      </select>
    </div>
    
    
    <div class="form-group col-md-3">
      <label for="lblRetornoTaxaDebito" id='lblRetornoTaxaDebito' >Taxa Débito Concorrente</label>
      <input type="text" placeholder='0,00%' required class="form-control" id="inputTaxaDebito" name="inputTaxaDebito"  disabled>
    </div>
    <div class="form-group col-md-3">
      <label for="lblRetornoDescontoDebito" id='lblRetornoDescontoDebito'>Desconto Débito</label>
      <input type="text"  placeholder='00,00%' required  class="form-control" id="inputDescontoDebito" name="inputDescontoDebito"  disabled>
    </div>
  </div>

  <div class="form-row">


<div class="form-group col-md-3">
  <label for="lblRetornoTaxaCredito" id='lblRetornoTaxaCredito'>Taxa Crédito Concorrente</label>
  <input type="text"  placeholder='0,00%' required  class="form-control" id="inputTaxaCredito"  name="inputTaxaCredito" disabled>
</div>
<div class="form-group col-md-3">
  <label for="lblRetornoDescontoCredito" id='lblRetornoDescontoCredito'>Desconto Crédito</label>
  <input type="text"  placeholder='00,00%' required  class="form-control" id="inputDescontoCredito" name="inputDescontoCredito" disabled>
</div>

<div class="form-group col-md-3">
 
  <center>  <label for="lblRetornoDescontoCredito" id='lblRetornoDescontoCredito'>Enviar Simulação</label><BR><button type="submit"     class="btn btn-primary" style="float: center;" >Enviar Simulação...</button></center>
  
</div>

</div>
<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>

  </div>


</div>

 

</form>
  
  </div>




<!-- COLOCANDO OS JS NO FIM DA PAGINA PARA UMA EXECUÇÃO MAIS RÁPIDA -->
 
<!-- JQUERY -->
<script src="http://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script src="js/jquery.cpfcnpj.js"></script>


<!-- bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.js"></script>


  
<script>



    // setando QUAIS campos recebem mascara pelo JQUERY
    // utilizo o evento onload porque só executa os metodos jquery APÓS todos os JAVASCRIPTS terem sido carregados, com o evento onReady do JQUERY não é possivel
 
    window.onload = function(){




    $('#inputTaxaDebito').mask('0,00'+'%');

    $('#inputDescontoDebito').mask('00,00'+'%');

    $('#inputTaxaCredito').mask('0,00'+'%');

    $('#inputDescontoCredito').mask('00,00'+'%');
    
    $("#inputTelefone").mask("(00) 0000-00009");


          
  $('#btnAutorizarEnvio').click(function(){

    $('#status').val('1');
    document.getElementById("myForm").submit();
  
  });

  $('#btnRejeitarEnvio').click(function(){

  $('#status').val('0');
  document.getElementById("myForm").submit();

  });





    
 }



 // Ao submeter o form  executa evento de Parar envio e abrir janela MODAL
 $('#myForm').on('submit', function(e) {

  //stop submit mostrando Janela Confirma
  e.preventDefault(); 
 
    $('#modalConfirma').modal('show');
 
});
 


</script>
<script src='js/myJquery.js'></script>

 

  <div class="modal fade" id="modalConfirma" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
               <h5>  Status da Proposta </h5>
            </div>
            <div class="modal-body">
                
                O que você deseja fazer?
                
            </div>
            <div class="modal-footer">
               <a href="#" id="btnRejeitarEnvio" class="btn btn-danger danger">Rejeitar</a>
                <a href="#" id="btnAutorizarEnvio" class="btn btn-success success">Aceitar</a>
            </div>
        </div>
    </div>
</div>


</body>

</html>