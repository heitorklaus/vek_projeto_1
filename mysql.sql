/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.32-MariaDB : Database - vek
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`vek` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `vek`;

/*Table structure for table `tb_propostas` */

DROP TABLE IF EXISTS `tb_propostas`;

CREATE TABLE `tb_propostas` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `concorrente` varchar(120) DEFAULT NULL,
  `cnpj` varchar(19) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `ramo` int(4) DEFAULT NULL,
  `taxa_debito` varchar(50) DEFAULT NULL,
  `desconto_debito` varchar(50) DEFAULT NULL,
  `taxa_credito` varchar(50) DEFAULT NULL,
  `desconto_credito` varchar(50) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `tb_propostas` */

/*Table structure for table `tb_ramo_atividade` */

DROP TABLE IF EXISTS `tb_ramo_atividade`;

CREATE TABLE `tb_ramo_atividade` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `ramo` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `limit_debito` decimal(10,2) DEFAULT NULL,
  `limit_credito` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=Aria AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin PAGE_CHECKSUM=1;

/*Data for the table `tb_ramo_atividade` */

insert  into `tb_ramo_atividade`(`id`,`ramo`,`limit_debito`,`limit_credito`) values (1,'Varejo de Roupas','2.20','2.50'),(2,'Alimentação Restaurantes','2.50','2.80'),(3,'Venda Maquinas Agrícolas','1.20','1.50'),(4,'Concessionaria de Carros','1.10','1.80');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
